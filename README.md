# Entorno de programación LOGO para la plataforma App Inventor

Gustavo Del Dago
 
## Panorama y contexto.

Una serie de motivos que exceden por mucho las posibilidades de ser tratados aquí, hicieron que los llamados "teléfonos celulares” se instalen de forma masiva en amplios sectores de la sociedad. Los diversos ámbitos de la vida moderna se han visto invadidos por una tecnología que se nos presenta de manera acabada, cerrada. Se trata de bienes de consumo, de productos cuyo diseño obedece más a las lógicas del marketing que a las necesidades o demandas de los futuros usuarios. La escuela no está exenta de este fenómeno que tiene carácter masivo y que no reconoce, hasta donde nos es dado observar, clases sociales. 

Por otra parte, ampliar la enseñanza de las ciencias de la computación y en particular de la programación a todos los estudiantes, se presenta como una estrategia muy poderosa para las naciones que aspiran a lograr una auténtica autonomía tecnológica. Alcanzar dicha autonomía tecnológica es requisito indispensable para poner el desarrollo científico y tecnológico al servicio de la sociedad que lo financia.

En este panorama, los programas de alfabetización digital, como nuestro programa *Conectar Igualdad*, el *Plan Ceibal* de nuestros hermanos uruguayos o el *Canaima educativo* desarrollado por los hermanos venezolanos, constituyen (o constituyeron) una plataforma tecnológica de gran valor en la medida que pusieron en las manos de millones de estudiantes (niños y niñas en edad escolar) computadoras portátiles. Sin ánimos de reducir un asunto muy complejo, podemos decir sin temor a equivocarnos que la condición de acceso (tener las computadoras) sin ser suficiente, es absolutamente necesaria.

En la actualidad el programa Conectar Igualdad está literalmente desarticulado (misma suerte parece que le toca correr, por el momento, a *Canaima educativo*). 

La presencia de “teléfonos celulares” en el aula, sin embargo, muestra un crecimiento constante.  Estudios realizados en la provincia de Tucumán, por citar un ejemplo, indican que más del 80% de los estudiantes, aún en los barrios más humildes, tienen teléfonos celulares.

Es en este contexto que, quienes estamos interesados en la enseñanza de la programación, debemos buscar alternativas que nos permitan enseñar y aprender los fundamentos de las ciencias de la computación, utilizando como vehículo las tecnologías disponibles. 

Cuando hablamos de pensamiento computacional y de programación, sabemos que la tecnología no es importante puesto que no es el objeto de estudio y, al mismo tiempo, se convierte en algo importante en la medida que sin acceso a determinada tecnología el estudio de esta disciplina encuentra, indefectiblemente, un límite. En definitiva, podemos ver que, si no nos detenemos a señalar la  perspectiva de análisis, estamos frente a una especie de paradoja: la tecnología es importante y no es importante al mismo tiempo.

El software se convierte en el producto concreto del trabajo de programación. Quienes consideramos que el trabajo de programación escolar, ese trabajo que se hace mientras se aprende a programar en la escuela, tiene como resultado (además de los aprendizajes relacionados con las ciencias de la computación) unos programas concretos, entendemos que estos programas constituyen auténticas *producciones escolares*. En este sentido, creo que es fundamental que dichas producciones escolares puedan circular más allá de los límites de la propia escuela. Familiares, amigos y compañeros, son el mejor público para este tipo de producciones, son estos consumidores quienes las podrán dotar de significados más auténticos.

### App Inventor

La posibilidad de programar “teléfonos celulares” tiene un atractivo innegable para quienes buscamos estrategias acordes al contexto que se acaba de describir. En pocas palabras, si los chicos y chicas elaboran programas de computación que se ejecutan en los teléfonos celulares, el número de  potenciales consumidores para las producciones escolares crecerá a tono con la instalación de estos dispositivos en los distintos ámbitos de la vida social.

Actualmente la tecnología de *App Inventor* constituye una puerta de entrada muy atractiva puesto que, desde un entorno de programación basado en bloques, permite generar aplicaciones que funcionan (se ejecutan) en dispositivos con sistema operativo *Android*.

*App Inventor*, que tiene una serie de características tan atractivas como prácticas, está fuertemente basado en un paradigma de programación que se puede definir como “orientado a eventos”. Sin entrar aquí en detalles, podemos decir que, desde el punto de vista de la didáctica de la programación, se trata de una seria limitación para quienes pretenden abordajes que no estén enmarcados en dicho paradigma.

### Una idea de para el trabajo final.

Con el propósito de superar esta limitación y fuertemente inspirado desde lo conceptual en los principios constructivistas presentes en el entorno *LOGO* y desde lo técnico en el módulo *Turtle Graphics* disponible en *Python*, estoy barajando la idea de desarrollar una extensión para la plataforma *App Inventor* que permita disponer de un entorno de programación *LOGO*.


Los ambientes LOGO y en particular aquellos orientados a los gráficos con la tortuga son numerosos y muy variados. Durante los años que no separan desde el momento fundacional de esta modalidad educativa (Originada con las ideas de Seymour Papert en 1966), se han desarrollado una gran diversidad de sistemas de computación. en la amplia mayoría se dispuso o se dispone de ambientes LOGO.

App inventor, una plataforma de gran difusión que permite programar aplicaciones para dispositivos móviles (los llamados celulares), propone un paradigma orientado e eventos. en este sentido, los programas que se desarrollan son, generalmente, una colección de rutinas o procedimientos de servicios. Cada uno de estos procedimientos atenderá algunos eventos determinados. 

Determinar el estado del arte exige contar con una serie de criterios que nos permitan orientar el trabajo de investigación y facilitar las instancias de análisis de los hallazgos.

Los criterios, en este caso, pueden señalarse describiendo las características que, de acuerdo con un enfoque determinado de la didáctica de la programación, son necesarias o deseables.

## Estado del arte    
La herramienta de desarrollo debe estar orientada a programadores aprendices y nóveles. Deberá ser adecuada para trabajar sobre conceptos computacionales (fundamentos) antes que sobre aplicaciones útiles desde el punto de vista de potenciales usuarios finales.

* A fines de ofrecer amplios espacios de circulación para los productos elaborados, éstos deberán funcionar en dispositivos móviles compatibles con Android. 
* El entorno de desarrollo debe funcionar en una computadora antes que en el propio dispositivo portátil. (un auténtico entorno de desarrollo cruzado)
* El código de programa deberá tener una representación gráfica (en el sentido que se emplea este término para referir a los sistemas tipo piezas encastrables típico de sistemas como Scratch, Pilas Bloques, App Inventor, Blockly, etc.). Se privilegia la posibilidad de programar en un ambiente libre de errores de sintaxis.
* La posibilidad de obtener o trabajar con representaciones textuales del código de programa, se considera deseables pero no necesaria.
* El entorno de trabajo no debe proponer o favorecer metáforas propias de las aplicaciones orientadas a la informática de gestión (formularios, campos, etc.).

En principio y hasta el momento no pude encontrar un sistema que reúna estas características. sin embargo, existen una serie de aplicaciones y tecnologías que ofrecen buenos puntos de partida o constituyen fuentes de ideas e inspiración.

* App Inventor ofrece todas las características que se esperan de un entorno de desarrollo cruzado.
* El módulo Turtle disponible en la biblioteca Python presenta una implementación potente de los gráficos de tortuga en un ambiente de programación Python.
* El sistema Turtlegraphics for Smartphones and Tablets de la Bern University of Teacher Education. Presenta una serie de características, desde lo didáctico, muy cercanas a las buscadas. Sin embargo, esta aplicación no permite programar utilizando bloques. Tampoco se trata de un producto desarrollado bajo los principios del SL de modo que la posibilidad de extender dicha plataforma queda descartada.
* La aplciación Turtle geometry. desarrolalda en Clojure, no esta orientada a la enseñanza de la programación sino a la exploración de ese terreno muy fértil donde computación y matemática se funden. en síntesis no se trata de una herramienta adecuada para nuestros propositos. 
